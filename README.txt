---Welcome to Precog---

Computer Science CMT3333 Software Development Project

Investigating The Efficiency of Crime Analysis Using Interactive Geographical Visualisations 
of Data 
 
Date: 21/04/2015 
Supervisor: Dr Chris Rooney 
Student Name: Joe Roberts 
Student ID Number: M00422701 
Campus:  Hendon




The final software program is a web based application and a preprocessing application using Java. The source 
files for these can be found in the precog folder along with the compiled classes in /out/. A 
jar has been created in the following url

	/precog/out/artifacts/precog_jar/PreCog.jar


The source files for the web application are located in the /src/web/ directory with the index 
html file found in /preog/. The web application is available online at the following URL: 

	map.precog.org.uk

If you wish to run the web app locally, you will need to run an apache server. If you have 
apache installed, copy the Precog folder into the apache's root folder. Run apache. Then 
within a browser, preferrably Chrome, type the following URL:

	localhost/precog/

If you use an alternative type of local server such as WAMP or MAMP copy the precog folder 
into the www directory and type the same URL.




The java program can be run however it requires the preinstallation of MySQL and the creation 
of four tables within a database called precog. This can be done by importing the precog.sql file located at the following location:

	precog/data/sql/precog.sql

To import open the MySQL terminal and type the following:

	-> USE precog;
	-> source location_of_Precog_DIR/data/sql/precog.sql

To run the jar, the jar takes a parametre which is the crime data .csv directory which are the 
folders found in /data/police_data/crimedata/. set the current dir to /precog/ then type the 
following in terminal.

	java -jar out/artifacts/PreCog_jar/PreCog.jar /location_of_csv_crime_data_files/


NOTE: Due to the nature of the Java program being a processor of large CSV file directories, 
the program can take up to 24 hours to complete.

NOTE: Compiling the program requires the use of a build tool to include dependencies. I 
initially used Maven to accomplish this however, it was then discovered that the ide I used 
(intellij idea) also provided a tool to add external jars. To create a runnable jar of this 
program through the ide the following jars need to be downloaded and added to dependencies 
through module settings.

	json-simple-1.1.1.jar
	mysql-connector-java-5.1.35.jar

To use maven add the following dependencies to the pom.xml file.

	<dependency>
		<groupId>mysql</groupId>
		<artifactId>mysql-connector-java</artifactId>
		<version>5.1.6</version>
	</dependency>

	<dependency>
		<groupId>com.googlecode.json-simple</groupId>
		<artifactId>json-simple</artifactId>
		<version>1.1.1</version>
	</dependency>