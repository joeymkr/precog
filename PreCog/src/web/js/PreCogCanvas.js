/**
 * Created by Joe Roberts on 20/02/2015.
 */

var gl;                     //global variable for the webgl context
var mvMatrix;
var perspectiveMatrix;      //matrix for view perspective
var colourAttribute;        //colour attribute used in shaders
var vertexPositionAttribute;//vertex attribute used in shaders
var pointSizeAttribute;     //point attribute used in shaders

var verticesColourBuffer;   //buffer used for vertex colour buffer
var verticesBuffer;         //vertices buffer
var pointBuffer;            //point buffer

var rotation = 0.0;
var rotateX = 0;
var rotateY = 1;
var x = 0.01;
var y = 0.01;
var z = -1.5;
var xOffSet = 0.025;
var yOffSet = -0.56;
var zOffSet = 0.90;

var grid = null;            //array of floats for crime data
var vertices = null;        //array of floats for country border
var countryZ = -0.9;

var cyclingDir;             //direction of cycling value

var colours = {
    "red": {'r': 237, 'g': 37, 'b': 37},
    "blue": {'r': 41, 'g': 137, 'b': 216}
};

var countryDrawable = true;
var meshType = "point";

var pointSize = 0.1;

var viewHeat = true;
var heatColors = [];
var heatMapPointSizes = [];

function glStart(){
    var canvas = document.getElementById("glCanvas");

    gl = initWebGL(canvas);     //initialise the graphics context

    if (gl){
        gl.clearColor(0.0,0.0,0.0,1.0);                     //Set clear colour to black, fully opaque
        gl.clearDepth(1.0);                                 //enable depth testing
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE);                 //allows use to blend with alpha
        gl.enable(gl.BLEND);
        gl.depthFunc(gl.LEQUAL);                            //near things obscure far things

        //inits the shaders
        initShaders();

        drawScene();

        //call routine that builds the objects to draw
        drawCountry();
        drawHeatMap();

        reDraw();
    }
}

function initWebGL(canvas){
    gl = null;

    try{
        //try to grab the standard context. if it fails, fallback to experimental
        gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
    } catch (e){
        alert("Unable to establish gl context.");
    }

    // if we don't have a graphics context, we give up
    if (!gl){
        alert("Unable to initialise webGL. your browser may not support it.")
        gl = null;
    }

    return gl;
}

function initShaders(){
    var fragmentShader = getShader(gl, "shader-fs");
    var vertexShader = getShader(gl, "shader-vs");

    shaderProgram = gl.createProgram();                 //uses webGL's object createProgram
    gl.attachShader(shaderProgram, vertexShader);       //attachs these 2 shaders to createProgram object
    gl.attachShader(shaderProgram, fragmentShader);     //
    gl.linkProgram(shaderProgram);                      //links shaders

    //if creating the shader program fails, alert

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)){
        alert("unable to initialize the shader program");
    }

    gl.useProgram(shaderProgram);

    vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    gl.enableVertexAttribArray(vertexPositionAttribute);

    colourAttribute = gl.getAttribLocation(shaderProgram, "aVertexColor");
    gl.enableVertexAttribArray(colourAttribute);

    pointSizeAttribute = gl.getAttribLocation(shaderProgram, "pointSize");
    gl.enableVertexAttribArray(pointSizeAttribute);
}

function getShader(gl, id){
    var shaderScript, theSource, currentChild, shader;

    shaderScript = document.getElementById(id);

    if (!shaderScript){
        return null;
    }

    theSource = "";
    currentChild = shaderScript.firstChild;

    while (currentChild){
        if(currentChild.nodeType == currentChild.TEXT_NODE){
            theSource += currentChild.textContent;
        }

        currentChild = currentChild.nextSibling;
    }

    //looks at the mime type of the object
    if(shaderScript.type == "x-shader/x-fragment"){
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if(shaderScript.type == "x-shader/x-vertex"){
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
        //unknown shader type
        return null;
    }

    gl.shaderSource(shader, theSource);

    //compile the shader program
    gl.compileShader(shader);

    //see if it compiled
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)){
        alert("An error occurred compiling shader: " + gl.getShaderInfoLog(shader));
        return null;
    }

    return shader;
}

function drawCountry(){
    //dont draw if country is off
    if (!countryDrawable){return;}

    // reduces the amount of AJAX calls
    if (vertices == null){
        vertices = getVertices(countryZ);
    }

    //country array is split into 23 separate sections, iterate through each
    for ( var i = 0; i < vertices.length; i++ ) {
        //create buffer for this array
        verticesBuffer = gl.createBuffer();
        //bind buffer to context
        gl.bindBuffer(gl.ARRAY_BUFFER, verticesBuffer);
        //link our array of data
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices[i]), gl.STATIC_DRAW);
        //point to shader attribute
        gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

        var countryColors = [];

        for (var n = 0; n < (vertices[i].length/3); n++){
            countryColors.push(8.0); // red
            countryColors.push(8.0); //green
            countryColors.push(8.0); //blue
            countryColors.push(1.0); //alpha
        }

        initColourBuffer(countryColors);


        draw("lineloop", vertices[i].length/3);
    }
}

function initColourBuffer(colourArr){
    //create buffer for this array
    verticesColourBuffer = gl.createBuffer();
    //bind it to context
    gl.bindBuffer(gl.ARRAY_BUFFER, verticesColourBuffer);
    //link our array of data
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colourArr), gl.STATIC_DRAW);
    //point to shader variable
    gl.vertexAttribPointer(colourAttribute, 4, gl.FLOAT, false, 0, 0);
}

function initPointBuffer(pointArr){
    pointBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, pointBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Uint8Array(pointArr), gl.STATIC_DRAW);
    gl.vertexAttribPointer(pointSizeAttribute, 1, gl.UNSIGNED_BYTE, false, 0, 0);
}

function drawMeshGrid(){

    //reduces the amount of AJAX calls
    if (grid == null){
        grid = getTerrainMesh();
    }

    verticesBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, verticesBuffer);

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(grid), gl.STATIC_DRAW);

    gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

    var dataColors = [];
    var pointSizes;

    for (var i = 0; i < (grid.length/3); i++){

        var n = (i * 3) + 2;
        var opacity =  ((0.02 * grid[n]))*-1 ;

        var colour = getColor(opacity*10000);

        dataColors.push(colour.r);          //red
        dataColors.push(colour.g);              //green
        dataColors.push(colour.b);              //blue
        dataColors.push(1.0);               //alpha
    }

    pointSizes = setPointArray(grid.length/3);
    initColourBuffer(dataColors);
    initPointBuffer(pointSizes);

    draw(meshType, (grid.length/3));
}

function setPointArray(length){
    //if drawing with gl.POINTS an array of point sizes is created
    var pointArr = []
    for (var i = 0; i < length; i++){
        pointArr.push(pointSize);
    }

    return pointArr;
}

function drawHeatMap(){

    if (grid == null){
        grid = getTerrainMesh();

        heatColors = [];
        heatMapPointSizes = [];

        for (var i = 0; i < (grid.length/3); i++){

            var n = (i * 3) + 2;
            var opacity =  ((0.02 * grid[n]))*-1 ;

            var colour = getColor(opacity*10000);

            heatColors.push(colour.r);          //red
            heatColors.push(colour.g);              //green
            heatColors.push(colour.b);              //blue
            heatColors.push(1.0);               //alpha
        }
    }

    //flattens vertices to same z value
    grid = flattenZ(grid);

    verticesBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, verticesBuffer);

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(grid), gl.STATIC_DRAW);

    gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);

    heatMapPointSizes = setPointArray(grid.length/3);

    initColourBuffer(heatColors);
    initPointBuffer(heatMapPointSizes);

    draw(meshType, (grid.length/3));
}

//creates gradient between two const colours set
function getColor(intensity) {

    var newCol = {};

    newCol.r = getDifference(colours.red.r, colours.blue.r);
    newCol.g = 0.1;
    newCol.b = getDifference(colours.red.b, colours.blue.b);


    function getDifference(col1, col2) {

        if (isHighestShown()){
            var tempCol = col1;
            col1 = col2;
            col2 = tempCol;
        }

        var diff =  Math.round(col1 + ((col2 - col1) * (intensity / 100)));

        if(diff < 0) diff = 0;
        if(diff > 255) diff = 255;

        return diff / 255;
    }

    return newCol;

}

function drawScene() {
    //clears screen of current pixels
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    //init perspective/ camera angle
    perspectiveMatrix = makePerspective(5, 1080.0 / 600.0, 0.1, 1000.0);
}

function draw(drawType, length){

    loadIdentity();
    mvTranslate([x, y, z]);

    mvPushMatrix();
    mvRotate(rotation, [rotateX, rotateY, 0]);
    mvTranslate([xOffSet, yOffSet, zOffSet]);

    setMatrixUniforms();

    if (drawType.toUpperCase() === "triangle".toUpperCase()) {
        gl.drawArrays(gl.TRIANGLES, 0, length);
    } else if (drawType.toUpperCase() === "lineloop".toUpperCase()){
        gl.drawArrays(gl.LINE_LOOP, 0, length);
    } else if (drawType.toUpperCase() === "point".toUpperCase()){
        gl.drawArrays(gl.POINTS, 0, length);
    } else if (drawType.toUpperCase() === "trianglestrip".toUpperCase()){
        gl.drawArrays(gl.TRIANGLE_STRIP, 0, length);
    } else if (drawType.toUpperCase() === "trianglefan".toUpperCase()){
        gl.drawArrays(gl.TRIANGLE_FAN, 0, length);
    } else if (drawType.toUpperCase() === "line".toUpperCase()){
        gl.drawArrays(gl.LINES, 0, length);
    }


    mvPopMatrix();
}

function setCyclingDir(dir){
    cyclingDir = dir;
}

//translate data through space
function animateCountry(){

    countryZ += cyclingDir;
    zOffSet -= cyclingDir;
    vertices = null;
    if(countryZ > 0.02){
        countryZ = -0.5;
        zOffSet = 0.48;
    } else if (countryZ < -0.5){
        countryZ = 0.02;
        zOffSet = -0.04;
    }
    reDraw();
}

function resetRotation(){
    rotation = 0.0;
}

function loadIdentity() {
    mvMatrix = Matrix.I(4);
}

function multMatrix(m){
    mvMatrix = mvMatrix.x(m);
}

//translate mathematics for vertices
function mvTranslate(v){
    multMatrix(Matrix.Translation($V([v[0], v[1], v[2]])).ensure4x4());
}

//inits matrices
function setMatrixUniforms(){
    var pUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
    gl.uniformMatrix4fv(pUniform, false, new Float32Array(perspectiveMatrix.flatten()));

    var mvUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
    gl.uniformMatrix4fv(mvUniform, false, new Float32Array(mvMatrix.flatten()));
}

var mvMatrixStack = []

//push onto matrix stack
function mvPushMatrix(m){
    if(m){
        mvMatrixStack.push(m.dup());
        mvMatrix = m.dup();
    } else{
        mvMatrixStack.push(mvMatrix.dup());
    }
}

//pops from matrix stack
function mvPopMatrix(){
    if(!mvMatrixStack.length){
        throw("Can't pop from an empty matrix stack.");
    }

    mvMatrix = mvMatrixStack.pop();
    return mvMatrix;
}

function mvRotate(angle, v){
    var inRadians = angle * Math.PI / 180.0;

    var m = Matrix.Rotation(inRadians, $V([v[0], v[1], v[2]])).ensure4x4();
    multMatrix(m);
}

function reDraw(){
    drawScene();
    drawCountry();
    if(viewHeat){
        drawHeatMap();
    }else{
        drawMeshGrid();
    }

}

function setCountryDrawable(){
    if (countryDrawable){
        countryDrawable = false;
        document.getElementById("borderToggle").innerHTML = "Country Border Off";
    }
    else {
        countryDrawable = true;
        document.getElementById("borderToggle").innerHTML = "Country Border On";
    }
}

function setMeshType (type){
    meshType = type;
}

function setDetail (detail){
    if (detail == 'low'){
        setLowDetail();
        pointSize = 2.0;
    } else {
        setHighDetail();
        pointSize = 0.1;
    }
    resetGrid();
}

function viewHeatMap(){
    if (!showHighestCrime){
        if(viewHeat){
            viewHeat = false;
            document.getElementById("heatMapToggle").innerHTML = "Height Map On";
            z = -1.5;
            countryZ = -0.5;
            zOffSet = 0.50;
            zoom(-1.0);
            vertices = null;
        } else {
            z = -1.5;
            viewHeat = true;
            document.getElementById("heatMapToggle").innerHTML = "Heat Map On";
        }
    } else {
        alert("Can not view heat map in Highest crime mode");
    }
    resetGrid();
    reDraw();
}

function resetGrid(){
    grid = null;
}

function zoom(direction){
    var currentZ = z;
    z += direction;
    if (z > -0.1399) {
        z = currentZ;
        alert("Can not zoom in anymore");
    }
}

function compensatePointSize(direction){
    pointSize += (direction * 2);
}

function moveHorizontal(direction){
    x += direction;
}

function moveVertical(direction){
    y += direction;
}

function rotate(direction, rotX, rotY){
    rotateX = rotX;
    rotateY = rotY;
    rotation += direction;
}