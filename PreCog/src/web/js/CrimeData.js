/**
 * Created by Joe Roberts on 16/03/2015.
 */

var xmlhttp;
var response;

var detailType = "mesh_data.json";

var showHighestCrime = false;
var divisionVar = 0.04;

var month = 'december-2010';

function getHttpRequestObject(){
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
}

function getTerrainMesh(){

    getHttpRequestObject();
    getDoc();

    var gridArr = [];
    var iterator = 0;

    for ( var i = 0; i < response["meshData"].length/3; i++){
        var zPos;
        gridArr.push(response["meshData"][iterator]);
        gridArr.push(response["meshData"][iterator+1]);
        if (showHighestCrime){
            zPos = -0 -(divisionVar * response["meshData"][iterator+2]);
            //creates limit to distance max and min
            if(zPos < -0.5){zPos = -0.5;}
            if (zPos > -0.5) {zPos = 1.0;}
        }
        else {
            zPos = -0 -(divisionVar / response["meshData"][iterator+2]);
            if(zPos < -0.4){zPos = -0.5;}
        }

        gridArr.push(zPos);
        iterator +=3;
    }
    return gridArr;
}

function isHighestShown(){
    return showHighestCrime;
}

function showHighest(){
    if (!viewHeat) {

        if (showHighestCrime) {
            showHighestCrime = false;
            document.getElementById("highestCrimeToggle").innerHTML = "Highest Crime Areas Off";
            divisionVar = 0.04;
            z = -1.0;
            zoom(-1.0);
        }
        else {
            showHighestCrime = true;
            document.getElementById("highestCrimeToggle").innerHTML = "Highest Crime Areas On";
            divisionVar = 3.0;
            z = -1.0;
            zoom(-0.4);
        }
        resetGrid();
    }else {
        alert("Can not view Highest crime in heat map mode");
    }
}

function setLowDetail(){
    detailType = "mesh_data_low.json";
}

function setHighDetail(){
    detailType = "mesh_data.json";
}

function setMonth(monthType){
    month = monthType;
    switch (month){
        case 'december-2010':
            document.getElementById("date").innerHTML= "01 - 31 December 2010";
            document.getElementById("totalCrimes").innerHTML= "474,198";
            break;
        case 'january-2011':
            document.getElementById("date").innerHTML= "01 - 31 January 2011";
            document.getElementById("totalCrimes").innerHTML= "514,506";
    }

}

function getDoc(){
    xmlhttp.open("GET", "../../data/intensity_data/" + month + "/" + detailType, false);
    xmlhttp.send();
    response = JSON.parse(xmlhttp.responseText);
}

function flattenZ(grid){
    for (var i = 0; i < grid.length/3; i++){
        var n = (i*3) + 2;

        grid[n] = countryZ;
    }

    return grid;
}