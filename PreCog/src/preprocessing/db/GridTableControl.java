package preprocessing.db;

import preprocessing.business.SpatialProcessor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Created by Joe Roberts on 26/03/2015.
 */
public class GridTableControl extends DataControl{

    private PreparedStatement preStatement = null;
    private Connection connection;

    public GridTableControl(String host, String user, String password) {
        super(host, user, password);
    }

    @Override
    public void insertRow(String[] query){
        connection = getConnection();
        try {
            preStatement = connection.prepareStatement("INSERT INTO mesh_data (x_coordinate, y_coordinate, inner_point_count) VALUES (?,?,?)");
            preStatement.setFloat(1, Float.parseFloat(query[0]));
            preStatement.setFloat(2, Float.parseFloat(query[1]));
            preStatement.setInt(3, Integer.parseInt(query[2]));
            preStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("can not execute query : " + query);
        }finally {
            try {
                if (preStatement != null){
                    preStatement.close();
                }
                if (connection != null){
                    connection.close();
                }
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    public JSONObject copyPointCountToJson(SpatialProcessor callBack){
        ResultSet result = null;
        Connection con = null;
        String query = "SELECT * FROM mesh_data WHERE inner_point_count > 0;";

        JSONObject jsonObject = new JSONObject();
        JSONArray list = new JSONArray();

        try{
            con = getConnection();
            statement = con.createStatement();
            result = statement.executeQuery(query);

            while(result.next()){
                list.add(result.getDouble("x_coordinate")/100);
                list.add(result.getDouble("y_coordinate")/100);
                double percent = (callBack.calculatePercent(result.getDouble("inner_point_count")));
                list.add(percent);
            }

            System.out.println(list.size());

            jsonObject.put("meshData", list);

            return jsonObject;
        }catch (SQLException ex){
            ex.printStackTrace();
        }finally {
            try {
                if (result != null){
                    result.close();
                }
                if (statement != null){
                    statement.close();
                }
                if (con != null){
                    con.close();
                }
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }

        return null;
    }
}
