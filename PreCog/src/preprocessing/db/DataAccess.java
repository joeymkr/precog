package preprocessing.db;

import java.io.File;

/**
 * Created by Joe Roberts on 25/02/2015.
 */
public interface DataAccess {
    public final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public final String DB_URL = "jdbc:mysql://localhost/precog";
    public final String USER = "root";
    public final String PASS = "spit69fire";

    public String[] getFileData();

    public String queryTable(String query);

    public File getRootDir();
}
