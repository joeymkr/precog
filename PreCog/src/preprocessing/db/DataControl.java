package preprocessing.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;

/**
 * Created by Joe Roberts on 25/02/2015.
 */
public abstract class DataControl implements DataAccess {

    private Connection con = null;
    protected Statement statement = null;
    private ResultSet result = null;
    private DataLoader loader;

    private String host, user, password;

    public DataControl(String host, String user, String password) {
        this.host = host;
        this.user = user;
        this.password = password;
    }

    public void connectSQL(){
        try{
            con = getConnection();
            statement = con.createStatement();
            result = statement.executeQuery("select version()");

            if(result.next()){
                System.out.println(result.getString(1));
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }finally {
            try {
                if (result != null){
                    result.close();
                }
                if (statement != null){
                    statement.close();
                }
                if (con != null){
                    con.close();
                }
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    protected Connection getConnection(){
        try{
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            return DriverManager.getConnection(host, user, password);

        }catch (SQLException ex){
            ex.printStackTrace();
            return null;
        }
    }

    public abstract void insertRow(String[] query);

    public void createLoader(File url)throws FileNotFoundException{

        loader = new DataLoader(url);
    }

    @Override
    public String queryTable(String query){
        result = null;
        try{
            con = getConnection();
            statement = con.createStatement();
            result = statement.executeQuery(query);

            if(result.next()){
                return result.getString(1);
            }
        }catch (SQLException ex){
            ex.printStackTrace();
        }finally {
            try {
                if (result != null){
                    result.close();
                }
                if (statement != null){
                    statement.close();
                }
                if (con != null){
                    con.close();
                }
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String[] getFileData(){

        if (loader.isFirstLine()){

            loader.loadData();//first line of file is not required
            loader.setFirstLineBoolean(false);

        }

        if (loader.hasNextLine()) {

            return loader.loadData();
        }

        return null;
    }

    @Override
    public File getRootDir(){
        return new File("/Users/david/personal-projects/PreCog-orig/data");
    }
}
