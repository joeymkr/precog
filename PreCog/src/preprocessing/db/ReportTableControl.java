package preprocessing.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;

/**
 * Created by Joe Roberts on 26/03/2015.
 */
public class ReportTableControl extends DataControl{

    private PreparedStatement preStatement = null;
    private Connection connection;

    public ReportTableControl(String host, String user, String password) {
        super(host, user, password);
    }

    @Override
    public void insertRow(String[] query){
        connection = getConnection();
        try {
            preStatement = connection.prepareStatement("INSERT INTO january (falls_within, longitude, latitude, location, crime_type) VALUES (?,?,?,?,?)");
            preStatement.setString(1, query[0]);
            preStatement.setFloat(2, Float.parseFloat(query[1]));
            preStatement.setFloat(3, Float.parseFloat(query[2]));
            preStatement.setString(4, query[3]);
            preStatement.setString(5, query[4]);
            preStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("can not execute query : " + query);
        }finally {
            try {
                if (preStatement != null){
                    preStatement.close();
                }
                if (connection != null){
                    connection.close();
                }
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
    }
}
