package preprocessing.db;

import java.io.*;
import java.util.*;


/**
 * Created by Joe Roberts on 25/02/2015.
 */
class DataLoader{
    boolean firstLine;
    Scanner reader;

    public DataLoader(File url)throws FileNotFoundException{
        reader = new Scanner(url);
        firstLine = true;
    }

    public boolean hasNextLine(){
        if (reader.hasNext()){
            return true;
        }
        return false;
    }

    public boolean isFirstLine(){
        return firstLine;
    }

    public void setFirstLineBoolean(boolean b){
        firstLine = b;
    }

    public String[] loadData(){

        reader.useDelimiter("\n");

        String line = reader.next().trim();

        // this is okay 'only' because data file is well-formed in strict order

        // note that it is important to trim off the newline

        // split the line with very simple regex

        String[] bits = line.split(",");


        return bits;
    }
}
