package preprocessing.business;

import preprocessing.db.DataControl;
import preprocessing.db.GridTableControl;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Joe Roberts on 25/02/2015.
 */

public class SpatialProcessor {
    private DataControl data;
    private double highPercent;

    public SpatialProcessor(DataControl dc){
        this.data = dc;
        highPercent = 0.0;
    }

    public void processMesh(){
        getHighestPoint();
        GridTableControl gridData = (GridTableControl) data;
        JSONObject jsonObject = gridData.copyPointCountToJson(this);

        try{
            FileWriter file = new FileWriter("data/intensity_data/january-2011/mesh_data.json");
            file.write(jsonObject.toJSONString());
            file.flush();
            file.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public double calculatePercent(double z){
        return (z / highPercent)*100;
    }

    private void getHighestPoint(){
        String query = "SELECT inner_point_count FROM mesh_data ORDER BY inner_point_count DESC LIMIT 1;";
        highPercent = Double.parseDouble(data.queryTable(query));
        System.out.println(highPercent);
    }
}
