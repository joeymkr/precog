package preprocessing.business;

import java.util.ArrayList;

/**
 * Created by Joe Roberts on 26/03/2015.
 */
public class GridGenerator {
    private double inc = 0.01;

    private double frameXLeftPos = -8.00;
    private double frameYBottomPos = 50.00;

    private double frameWidth = 10.00;
    private double frameHeight = 11.00;

    private double gridX = frameXLeftPos;
    private double gridY = frameYBottomPos;

    private ArrayList<Double> grid;

    private int verticesCount = 0;

    public ArrayList<Double> createGrid(){
        grid = new ArrayList<Double>();
        loadPoints();
        return grid;
    }

    private void loadPoints(){
        verticesCount = 0;
        for (double column = 0.0; column < frameHeight; column += inc){
            for (double row = 0.0; row < frameWidth; row += inc){

                grid.add(gridX);
                grid.add(gridY);
                grid.add(-0.01);
                gridX += inc;
                verticesCount += 1;
            }
            gridX = frameXLeftPos;
            gridY += inc;
        }
    }

    public int getVerticesCount(){
        return verticesCount;
    }
}
