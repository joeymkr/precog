package preprocessing.business;

import preprocessing.db.DataControl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by Joe Roberts on 16/03/2015.
 */
public class PreProcessor {

    private DataControl data;
    private File[] dirList;

    public PreProcessor(DataControl data){
        this.data = data;
    }

    public void loadDirList(String directoryPath){

        File dir = new File(directoryPath);

        if (!dir.exists()) {
            throw new RuntimeException("Data directory doesn't exist");
        }

        dirList = dir.listFiles();
        System.out.println("Directory list length " + dirList.length);

    }

    public void processDir(){

        String[] query;
        String[] fileData;
        for(File file : dirList){
            System.out.println("Loading file: " + file.getName());
            try {
                data.createLoader(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            while ((fileData = data.getFileData()) != null){
                if (checkDataIsFormatted(fileData)) {
                    query = new String[]{"'" + fileData[3] + "'", fileData[4], fileData[5], "'" + fileData[6] + "'", "'" + fileData[9] + "'"};

                    data.insertRow(query);
                }
            }

        }
    }

    private boolean checkDataIsFormatted(String[] arr){
        String[] dataArr = new String[]{arr[3],arr[4],arr[5],arr[6],arr[9]};    //takes only the data we need from filedata
        for (String data: dataArr){
            if (data == null || data.equals("")){
                return false;
            }
        }
        return true;
    }

    public void processSpatialIntensity(){
        GridGenerator gridGen = new GridGenerator();
        ArrayList<Double> generatedGrid = gridGen.createGrid();
        gridGen = null;

        int pointCount = (generatedGrid.size()/3);
        Double[][] points = new Double[pointCount][3];
        int gridIterator = 0;

        for (int i = 0; i < pointCount; i++) {
            gridIterator = 3 * i;
            points[i][0] = generatedGrid.get(gridIterator);
            points[i][1] = generatedGrid.get(gridIterator+1);
            points[i][2] = generatedGrid.get(gridIterator+2);
        }

        double highestX;
        double highestY;
        double lowestX;
        double lowestY;

        double pointRadius = 0.005;

        String result;

        for (int i = 0; i < pointCount; i++){
            highestX = (points[i][0] + pointRadius);
            lowestX = (points[i][0] - pointRadius);

            highestY = (points[i][1] + pointRadius);
            lowestY = (points[i][1] - pointRadius);

            String query = "SELECT COUNT(*) FROM january WHERE " +
                    "(longitude < " + highestX + " AND longitude > " + lowestX + ")" +
                    " AND " +
                    "(latitude < " + highestY + " AND latitude > " + lowestY + ");";

            result = data.queryTable(query);

            if (result != null) {
                data.insertRow(new String[]{String.valueOf(points[i][0]),
                                            String.valueOf(points[i][1]),
                                            result});

                System.out.println("Section " + i + " : " + result);
            }
        }
    }
}
