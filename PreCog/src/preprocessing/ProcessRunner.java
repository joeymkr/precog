package preprocessing;

import preprocessing.business.PreProcessor;
import preprocessing.business.SpatialProcessor;
import preprocessing.db.DataControl;
import preprocessing.db.GridTableControl;
import preprocessing.db.ReportTableControl;

public class ProcessRunner {

    public static void main(String[] args){
        System.out.println("Arguments length " + args.length);

        String dbHost = "jdbc:mysql://localhost/precog";
        String dbUser = "root";
        String dbPass = "";

        String dataDirectory = args[0];

        if (args.length > 1) {
            dbHost = args[1];
        }

        if (args.length > 2) {
            dbUser = args[2];
        }

        if (args.length > 3) {
            dbPass = args[3];
        }

        if (args.length > 2) {
            dbPass = args[3];
        }

        if (dataDirectory == null || dataDirectory.length() == 0) {
            throw new IllegalArgumentException("Please specify the data directory");
        }

        if (dbHost == null || dbHost.length() == 0) {
            throw new IllegalArgumentException("Please specify the database host");
        }

        if (dbUser == null || dbUser.length() == 0) {
            throw new IllegalArgumentException("Please specify the database user");
        }

        System.out.println("Database details { host: " + dbHost + ", user: " + dbUser + ", pass: " + dbPass + "}");

        DataControl dc = new ReportTableControl(dbHost, dbUser, dbPass);
        dc.connectSQL();

        PreProcessor preProcessor = new PreProcessor(dc);
        preProcessor.loadDirList(dataDirectory);
        preProcessor.processDir();
        preProcessor.processSpatialIntensity();

        DataControl gridDc = new GridTableControl(dbHost, dbUser, dbPass);
        gridDc.connectSQL();

        SpatialProcessor tprocessor = new SpatialProcessor(gridDc);
        tprocessor.processMesh();

    }
}
