<?php

    $string = file_get_contents("../../database/country_border/ukborderJSON.json");

    //decodes json file
    $jsonArray = json_decode($string, true);

    //gets the size of the first array dimension
    $topLayerArray = sizeof($jsonArray['coordinates']);

    //bottom layer dimension is always 2, Long & Lat
    $bottomLayerArray = 2;

    //used as iteration flag
    $secondLayerHasNext = true;
    $thirdLayerHasNext = true;

    $b = 0;
    $c = 0;

    for ( $a = 0; $a < $topLayerArray; $a++){

        while ( $secondLayerHasNext ){

            $thirdLayerArray = sizeof($jsonArray['coordinates'][$b]);

            while ( $thirdLayerHasNext ){

                for ( $d = 0; $d < $bottomLayerArray; $d++){

                    $newValue = ($jsonArray['coordinates'][$a][$b][$c][$d] / 100);

                    $jsonArray['coordinates'][$a][$b][$c][$d] = $newValue;
                }

                try {

                     $c = $c + 1;

                     checkNextElement($jsonArray, $a, $b, $c, 0);

                } catch (Exception $e){

                     $c = 0;
                     $thirdLayerHasNext = false;
                }
            }

            try {

                $b = $b + 1;

                checkNextElement($jsonArray, $a, $b, 0, 0);

            } catch (Exception $e){

                $b = 0;
                $secondLayerHasNext = false;
            }
        }

        $secondLayerHasNext = true;
        $thirdLayerHasNext = true;
    }

    echo "<p>success</p>";
    //writes new json array to json file
    file_put_contents('modifiedJson.json', json_encode($jsonArray));

    function checkNextElement($array, $m, $n, $o, $p){

        if (!$array['coordinates'][$m][$n][$o][$p]){

            throw new Exception();
        }
    }

?>