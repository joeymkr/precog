/**
 * Created by Joe Roberts on 02/03/2015.
 */

var xmlhttp = new XMLHttpRequest();
var ajaxResponse;
var numberOfCoordinates;

function getCoordinateTotal(){
    return numberOfCoordinates;
}

function request(doc){

    xmlhttp.open("GET", doc, false);
    xmlhttp.send();
}

function getVertices(depth){

    request("../../data/country_border/modifiedJson.json");

    ajaxResponse = JSON.parse(xmlhttp.responseText);
    return retrieveCoordinates(depth);
}

function retrieveCoordinates(depth){
    numberOfCoordinates = 0;

    topLayerArray = ajaxResponse['coordinates'].length;

    //bottom layer dimension is always 2, Long & Lat
    bottomLayerArray = 2;

    //used as iteration flag
    secondLayerHasNext = true;
    thirdLayerHasNext = true;

    b = 0;
    c = 0;

    zAxis = depth;

    var landBorder = [];
    var borderArr = [];

    for ( a = 0; a < topLayerArray; a++ ){  //each top level array is an island within the UK

        while ( secondLayerHasNext ) {

            while (thirdLayerHasNext) {
                numberOfCoordinates += 1;
                landBorder.push(ajaxResponse['coordinates'][a][b][c][0]);
                landBorder.push(ajaxResponse['coordinates'][a][b][c][1]);
                landBorder.push(zAxis);

                try {

                    c = c + 1;

                    checkNextElement(ajaxResponse, a, b, c, 0);

                } catch (e) {

                    c = 0;
                    thirdLayerHasNext = false;
                }


            }

            try {

                b = b + 1;

                checkNextElement(ajaxResponse, a, b, 0, 0);

            } catch (e) {

                b = 0;
                secondLayerHasNext = false;
            }
        }

        borderArr.push(landBorder);
        landBorder = [];

        secondLayerHasNext = true;
        thirdLayerHasNext = true;
    }

    return borderArr;
}

function checkNextElement(arr, m, n, o, p) {

    if (!arr['coordinates'][m][n][o][p]){

        throw new Exception();
    }
}
